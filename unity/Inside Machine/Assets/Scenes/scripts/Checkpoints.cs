﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoints : MonoBehaviour
{
    public GameObject player;
    public GameObject Checkpoint;


    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    public void Spawn()
    {

        Instantiate(player, new Vector2(Checkpoint.transform.position.x, Checkpoint.transform.position.y), Quaternion.identity);
    }
}
