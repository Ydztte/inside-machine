﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DegatsGestion : MonoBehaviour

{
    private Checkpoints checkPoints;
    void Start()
    {
        checkPoints = FindObjectOfType(typeof(Checkpoints)) as Checkpoints;
        //checkPoints = GetComponent<checkPoints>();
    }
    // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Obstacle")
        {
            Debug.Log("ApplyDamage 10");
            Destroy(gameObject);
              checkPoints.Spawn();


        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
