﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doors : MonoBehaviour
{

    Vector2 destination;



    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {


                if (this.name == "Door1")
                {
                    destination = GameObject.Find("Door2").transform.position;
                }
                else
                {
                    destination = GameObject.Find("Door1").transform.position;
                }


                other.transform.position = destination - Vector2.left * 3;


        }
    }
}
