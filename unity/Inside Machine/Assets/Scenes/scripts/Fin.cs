﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fin : MonoBehaviour
{
    public GameObject fin1;
    public GameObject fin2;
    public GameObject fin3;
    public GameObject Player;
    public GameObject player;

    private void Awake()
    {
        fin1.SetActive(false);
        fin2.SetActive(false);
        fin3.SetActive(false);
        Player.SetActive(true);


    }

    // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")

            GameObject.FindGameObjectWithTag("Player").SetActive(false); 
            StartCoroutine(FinWIn());

    }

    IEnumerator FinWIn()
    {

        Debug.Log("Debut coroutine");
        fin1.SetActive(true);
        yield return new WaitForSeconds(.9f);
        fin2.SetActive(true);
        fin3.SetActive(true);
        yield return null;
        
    }

}
