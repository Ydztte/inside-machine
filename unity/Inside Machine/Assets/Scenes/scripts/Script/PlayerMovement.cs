﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public CharacterController2D controller;
    public float horizontalMove;
    public float runSpeed = 40f;
    bool jump = false;
    public bool walking = false;

    public Animator animator;

    void Start()
    {
    walking = false;
}

    

    private void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if (walking == false)
        {
            animator.SetBool("IsWalking", false);
        }else
        {
            animator.SetBool("IsWalking", true);
        }
        if (jump == false)
        {
            animator.SetBool("IsJumping", false);
        }
        else
        {
            animator.SetBool("IsJumping", true);
        }

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
            walking = false;

  


        }
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime,false,jump);

        jump = false;

        if (horizontalMove == 0)
        {
            walking = false;
        }
        else
        {
            walking = true;
        }


    }
}
