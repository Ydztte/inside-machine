﻿using System.Collections;
using System.Collections;
using UnityEngine;

public class Tir : MonoBehaviour
{
    public GameObject projectile;
    public GameObject trfsShot;

    public float fireRate;


    private Coroutine fire = null;
    private bool boolAttack;

    public Transform playerTrfs;
    public float Bullet_Forward_Force;


    // Start is called before the first frame update
    void Start()
    {



    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector2.Distance(this.transform.position, playerTrfs.transform.position);

        if (distance >= 50000000000f)
        {
            boolAttack = false;
        }
        else
        {
            boolAttack = true;
        }

        if (fire == null)
        {
            fire = StartCoroutine(Shoot());
        }

    }



    IEnumerator Shoot()
    {



        while (boolAttack)
        {
            GameObject Temporary_Bullet_Handler;
            Temporary_Bullet_Handler = Instantiate(projectile, trfsShot.transform.position, trfsShot.transform.rotation) as GameObject;


            Temporary_Bullet_Handler.transform.Rotate(Vector2.down);

            Rigidbody2D Temporary_RigidBody;
            Temporary_RigidBody = Temporary_Bullet_Handler.GetComponent<Rigidbody2D>();

            //Tell the bullet to be "pushed" forward by an amount set by Bullet_Forward_Force.

            if(Temporary_RigidBody != null)
            {
               // Debug.Log("trigger");
             Temporary_RigidBody.velocity = (transform.forward * Bullet_Forward_Force);

            }


            //Basic Clean Up, set the Bullets to self destruct after 10 Seconds, I am being VERY generous here, normally 3 seconds is plenty.
            Destroy(Temporary_Bullet_Handler, 3.0f);

            yield return new WaitForSeconds(fireRate);

        }

        fire = null;

    }
}
