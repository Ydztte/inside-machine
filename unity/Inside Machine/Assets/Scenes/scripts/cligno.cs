﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cligno : MonoBehaviour
{

    public float timeRate;

    private Coroutine fire;

    public Sprite[] Mat;


    // Start is called before the first frame update
    void Start()
    {
        fire = StartCoroutine(Shoot());


    }


    IEnumerator Shoot()
    {



        while (true) { 
            
                this.GetComponent<SpriteRenderer>().sprite = Mat[1];
                this.GetComponent<Collider2D>().enabled = false;


            yield return new WaitForSeconds(timeRate);


            this.GetComponent<SpriteRenderer>().sprite = Mat[0];
                this.GetComponent<Collider2D>().enabled = true;
            

            yield return new WaitForSeconds(timeRate);

        }


    }
}

