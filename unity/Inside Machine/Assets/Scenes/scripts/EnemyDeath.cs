﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeath : MonoBehaviour
{

    private Checkpoints checkPoints;


    public GameObject Ennemi;
    public GameObject Player;
    public GameObject limite1;
    public GameObject limite2;



    void Start()
    {
        checkPoints = FindObjectOfType(typeof(Checkpoints)) as Checkpoints;
        //checkPoints = GetComponent<checkPoints>();
    }

    /* void OnCollisionEnter2D(Collision2D col)
     {
         if (col.gameObject.tag == "Player")
         {
             {
                 if(this.name == "ContactD1")
                 {
                     Destroy(Player);
                     checkPoints.Spawn();

                 }
                 else if(this.name == "ContactD2")
                 {

                     Destroy(Player);
                     checkPoints.Spawn();
                 }

             }
         }
     }*/
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            {
                if (this.name == "ContactD2")
                {
                    Destroy(Ennemi);
                }
                else
                {
                    Destroy(Player);
                    checkPoints.Spawn();
                }
            }

        }
    }
}
  

