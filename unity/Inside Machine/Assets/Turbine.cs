﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turbine : MonoBehaviour
{
    public GameObject TurbineWorking;
    public GameObject E;
    public bool isClose = false;
    public GameObject fumée;


    void Start()
    {
        E.SetActive(false);
        TurbineWorking.SetActive(true);
        isClose = false;
        fumée.SetActive(false);
    }
    void OnTriggerEnter2D(Collider2D col)
    {


        if (col.gameObject.tag == "Player")
            
        {
            Debug.Log("Entre");
            E.SetActive(true);
            isClose = true;

        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        Debug.Log("Sort");
        E.SetActive(false);
        isClose = false;
    }

    void Update()
    {


        if (Input.GetKeyDown(KeyCode.E))
        { 
             if (isClose == true) { 
                Debug.Log("E");
                TurbineWorking.SetActive(false);
                fumée.SetActive(true);
            }
            
        }
               

    }
}
